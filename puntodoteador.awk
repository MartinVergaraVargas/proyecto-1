BEGIN{

	print "digraph G {"
	print "rankdir = LR ;"
	print "size ="8 ","5""
    
}

{
    datos=$2
HIS 

    if(datos != datos2){

        print ""
        print "node [shape=box]"
        print datos " [label = " $2 "]"

        if ($3 == cadena){
            print datos " -> " datos2 "[label = cadena "$3 "]"
        }

        print ""
        print "node [shape=circle]"
    }

    print $1$2 " [label = " $1 "]"
    print $1$2 " -> " datos "[arrowhead = normal]"
    
    cadena=$3
    datos2=datos
    
}
END{
    print "}"
}
